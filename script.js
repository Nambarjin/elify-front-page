$(function() {
    var payout_handle = $("#payout-handle");
    $("#payout-slider").slider({
        value: 10,
        min: 10,
        max: 60,
        step: 10,
        create: function() {
            payout_handle.text("$" + $(this).slider("value"));
            $("input[name='payout-slider']").val($(this).slider("value"));
            calculatePayout();
        },
        slide: function(event, ui) {
            payout_handle.text("$" + ui.value);
            $("input[name='payout-slider']").val(ui.value);
            calculatePayout();
        }
    });
    var net_dup_handle = $("#network-duplicate-handle");
    $("#network-duplication-slider").slider({
        value: 10,
        min: 10,
        max: 50,
        step: 5,
        create: function() {
            net_dup_handle.text($(this).slider("value") + "%");
            $("input[name='network-duplication-slider']").val($(this).slider("value"));
            calculatePayout();
        },
        slide: function(event, ui) {
            net_dup_handle.text(ui.value + "%");
            $("input[name='network-duplication-slider']").val(ui.value);
            calculatePayout();
        }
    });
    var mom_ret_handle = $("#mom-retention-handle");
    $("#mom-retention-slider").slider({
        value: 10,
        min: 10,
        max: 70,
        step: 10,
        create: function() {
            mom_ret_handle.text($(this).slider("value") + "%");
            $("input[name='mom-retention-slider']").val($(this).slider("value"));
            calculatePayout();
        },
        slide: function(event, ui) {
            mom_ret_handle.text(ui.value + "%");
            $("input[name='mom-retention-slider']").val(ui.value);
            calculatePayout();
        }
    });
    var monthly_refer_handle = $("#monthly-refer-handle");
    $("#monthly-refer-slider").slider({
        value: 1,
        min: 1,
        max: 8,
        step: 1,
        create: function() {
            monthly_refer_handle.text($(this).slider("value"));
            $("input[name='monthly-refer-slider']").val($(this).slider("value"));
            calculatePayout();
        },
        slide: function(event, ui) {
            monthly_refer_handle.text(ui.value);
            $("input[name='monthly-refer-slider']").val(ui.value);
            calculatePayout();
        }
    });
    calculatePayout();
    $(".mobile-nav-toggle").click(function() {
        if($(".mobile-nav").hasClass("active")) {
            $(".mobile-nav").removeClass("active");
        } else {
            $(".mobile-nav").addClass("active");
        }
    });
});
function modalOpen(dialog) {
    $(dialog).fadeIn();
    $(dialog + " .modal-overlay").css({opacity: 1});
}
function modalClose(dialog) {
    $(dialog + " .modal-overlay").css({opacity: 0});
    $(dialog).fadeOut();
}
function calculatePayout() {
    var payout = parseInt($("#payout-slider input[name='payout-slider']").val());
    var refferal = parseInt($("#monthly-refer-slider input[name='monthly-refer-slider']").val());
    var retention = parseInt($("#mom-retention-slider input[name='mom-retention-slider']").val());
    var network = parseInt($("#network-duplication-slider input[name='network-duplication-slider']").val());
    var i = 1;
    var vars;
    while(i < 13) {
        vars = calculate_func(refferal, retention, network, i, payout);
        var total = Math.round(vars.total);
        var cash = Math.round(vars.cash);
        $("#calculation #net-" + (i)).html(total);
        $("#calculation #est-" + (i)).html("$" + cash);
        i++;
    }
}
function calculate_func(refferal, retention, network, month, payout) {
    var ret_val = {total: 0, cash: 0};
    var f_value = payout; //  10; //$10.00
    var quick_start = 0;
    var i = 1;
    // if (month <= 3) {
    //     quick_start = refferal * f_value;
    // }
    if (month === 3) {
        if (refferal * f_value * 2 >= f_value * 10) {
            if(month <= 3) {
                quick_start = refferal * f_value;
            }
        }
    }
    // if (ret_val.cash >= 1) {
    //     ret_val.cash = ret_val.cash + 0;
    // } else {
    //     ret_val.cash = ret_val.cash + (calculate_refercount(refferal, retention, network, month, 1) * f_value * 0.1);
    // }

    var recMemCom = 0;

    if(quick_start >= 1) {
        recMemCom = 0;
    } else {
        recMemCom = calculate_refercount(refferal, retention, network, month, i) * f_value * 0.1;
    }

    recMemCom = recMemCom + (Math.round(calculate_refercount(refferal, retention, network, month, 2)) * f_value * 0.08) +
                            (Math.round(calculate_refercount(refferal, retention, network, month, 3)) * f_value * 0.15) +
                            (Math.round(calculate_refercount(refferal, retention, network, month, 4)) * f_value * 0.05) +
                            (Math.round(calculate_refercount(refferal, retention, network, month, 5)) * f_value * 0.05) +
                            (Math.round(calculate_refercount(refferal, retention, network, month, 6)) * f_value * 0.08);


    // ret_val.cash = ret_val.cash + (
    //         (Math.round(calculate_refercount(refferal, retention, network, month, 2)) * f_value * 0.08) +
    //         (Math.round(calculate_refercount(refferal, retention, network, month, 3)) * f_value * 0.15) +
    //         (Math.round(calculate_refercount(refferal, retention, network, month, 4)) * f_value * 0.05) +
    //         (Math.round(calculate_refercount(refferal, retention, network, month, 5)) * f_value * 0.05) +
    //         (Math.round(calculate_refercount(refferal, retention, network, month, 6)) * f_value * 0.08)
    //         );
    ret_val.cash = quick_start + recMemCom;
    while (i < 7) {
        ret_val.total = ret_val.total + calculate_refercount(refferal, retention, network, month, i);
        i++;
    }
    return ret_val;
}
function calculate_refercount(refferal, retention, network, month, ref_num) {
    if (month === 1) {
        if (ref_num === 1) {
            return refferal;
        }
    }
    if (month === 2) {
        if (ref_num === 1) {
            return refferal + (refferal * retention / 100);
        }
        if (ref_num === 2) {
            return refferal * network / 100 * refferal;
        }
    }
    if (month > 2 && month < 13) {
        if (ref_num === 1) {
            var a1 = month;
            a1--;
            return refferal + (calculate_refercount(refferal, retention, network, a1, 1) * retention / 100);
        } else {
            var a1 = month;
            var a2 = ref_num;
            a1--;
            a2--;
            return (calculate_refercount(refferal, retention, network, a1, a2) * network / 100 * refferal) + (calculate_refercount(refferal, retention, network, a1, ref_num) * retention / 100);
        }
    }
    return 0;
}

